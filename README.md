Task

1. Open gmail & login

2. Click "Compose" button

3. Fill "To", "Subject" & "message" fields

4. Click "send" button

5. Verify that message is in "sent" folder

6. Remove message from the "sent" folder

7. Remove Google account


To run the ta-hw-mobile-android, you need:

 - run Appium on your computer and connect mobile
 
 - run this command: "mvn clean test" (you will use default mobile - "HTC One M9");

 - if not "HTC One M9" mobile will be use,  in order to change mobile you have to add

property "-Dmobile=HTC One M9". At the moment "HTC One M9" is supported.;


To get a generated report for ta-hw-mobile-android, you need:

 - run this command: "mvn allure:serve".