package ua.com.epam.test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.testng.annotations.Test;
import ua.com.epam.businessobject.GmailAccountSettingBO;
import ua.com.epam.businessobject.GmailHomeBO;
import ua.com.epam.businessobject.GmailLoginBO;
import ua.com.epam.businessobject.GmailSentMessageBO;

import java.net.MalformedURLException;

import static org.testng.Assert.assertTrue;

public class GmailSendMessageTest extends BaseTest {
    final String TOPIC = getRandomNumber();

    @Epic("Test send Gmail message.")
    @Description("Sign in to Gmail. Send message and delete after verification. Delete Google Account.")
    @Test
    public void gmailSentMessage() throws MalformedURLException {

        GmailHomeBO home = new GmailLoginBO()
                .goToLoginGoogle()
                .loginToGmail(dataProp.getLogin(), dataProp.getPassword());

        home.composeMsg()
                .writeAndSendMessage(dataProp.getRecipient(), TOPIC);

        GmailSentMessageBO sentMessage = home.toSentLetter();

        assertTrue(sentMessage.getTextMsg().contains(TOPIC),
                String.format("Message with topic - '%s' wasn't sent.", TOPIC));
        sentMessage.deleteMsg();

        new GmailAccountSettingBO().deleteAccountGmail();
    }
}
