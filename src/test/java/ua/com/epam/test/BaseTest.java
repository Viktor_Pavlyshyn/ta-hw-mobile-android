package ua.com.epam.test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import ua.com.epam.driver.LocalDriverManager;
import ua.com.epam.listener.AllureListener;
import ua.com.epam.utils.DataPropGmail;

import java.util.Random;

@Listeners({AllureListener.class})
public class BaseTest {
    protected DataPropGmail dataProp;

    @BeforeMethod
    protected void setUp() {
        dataProp = new DataPropGmail();
    }

    @AfterMethod
    protected void tearDown() {
        LocalDriverManager.closeDriver();
    }

    protected String getRandomNumber() {
        Random rd = new Random();
        return String.valueOf(rd.nextInt((9999999 - 1) + 1) + 1);
    }
}
