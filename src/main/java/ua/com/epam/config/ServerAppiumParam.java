package ua.com.epam.config;

public interface ServerAppiumParam {
    String IMPLICITLY_TIME = "25";
    String DEVICE_URL_PROPERTY = "http://localhost:4723/wd/hub";
}
