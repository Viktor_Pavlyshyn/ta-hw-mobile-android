package ua.com.epam.device;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.model.Mobile;
import ua.com.epam.utils.DataReader;

@Log4j2
public class DevicesManager {

    public static Mobile createMobile() {
        Mobile mobile = null;
        String deviceName = System.getProperty("mobile", "HTC One M9");

        log.error("Getting mobile by device name - " + deviceName);
        if (isSame(deviceName, "HTC One M9")) {
            mobile = DataReader.getMobileByDeviceName(deviceName);
        } else {
            log.error("This is an incorrect mobile input - " + deviceName);
            throw new RuntimeException("This is an incorrect mobile input - " + deviceName);
        }
        return mobile;
    }

    private static boolean isSame(String val1, String val2) {
        return val1 != null && val1.equalsIgnoreCase(val2);
    }
}
