package ua.com.epam.utils;

import static ua.com.epam.config.GmailProperties.*;

public class DataPropGmail extends PropertiesReaderDB {

    public String getRecipient() {
        return props.getProperty(RECIPIENT);
    }

    public String getLogin() {
        return props.getProperty(LOGIN_GMAIL);
    }

    public String getPassword() {
        return props.getProperty(PASSWORD_GMAIL);
    }
}
