package ua.com.epam.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.model.Mobile;
import ua.com.epam.utils.helper.LocalDateAdapter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ua.com.epam.config.DataPath.GMAIL_PATH_TO_MOBILES;

@Log4j2
public class DataReader {
    private static Gson g = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateAdapter()).create();

    public static List<Mobile> getMobiles() {
        log.info("Try to find mobiles.");

        List<Mobile> mobiles = new ArrayList<>();

        try (Stream<String> lines = Files.lines(Paths.get(GMAIL_PATH_TO_MOBILES))) {
            mobiles = lines.map(s -> g.fromJson(s, Mobile.class)).collect(Collectors.toList());
            if (mobiles.isEmpty()) {
                log.error("File by path - '{}' is empty!", GMAIL_PATH_TO_MOBILES);
                throw new RuntimeException("File is empty!");
            }
        } catch (IOException e) {
            log.error(String.format("Can't read file by path - %s.", GMAIL_PATH_TO_MOBILES), e);
        }

        log.info(mobiles.size() + " mobiles found!");
        return mobiles;
    }

    public static Mobile getMobileByDeviceName(String deviceName) {
        return getMobiles().stream()
                .filter(mobile -> deviceName.equals(mobile.getDeviseName()))
                .findAny()
                .orElse(null);
    }
}
