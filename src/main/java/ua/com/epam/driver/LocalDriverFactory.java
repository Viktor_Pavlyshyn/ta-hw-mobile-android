package ua.com.epam.driver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.Capabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static ua.com.epam.config.ServerAppiumParam.DEVICE_URL_PROPERTY;
import static ua.com.epam.config.ServerAppiumParam.IMPLICITLY_TIME;

@Log4j2
public class LocalDriverFactory {

    public static AndroidDriver createDriver(Capabilities capabilities) throws MalformedURLException {
        AndroidDriver<MobileElement> driver;

        log.info("Try to create AndroidDriver.");
        driver = new AndroidDriver<>(new URL(DEVICE_URL_PROPERTY), capabilities);
        driver.manage().timeouts().implicitlyWait(Long.parseLong(IMPLICITLY_TIME), TimeUnit.SECONDS);

        log.info("AndroidDriver was created.");
        return driver;
    }
}
