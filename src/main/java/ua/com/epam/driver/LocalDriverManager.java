package ua.com.epam.driver;

import io.appium.java_client.android.AndroidDriver;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;

import static ua.com.epam.driver.LocalDriverFactory.createDriver;

@Log4j2
public class LocalDriverManager {
    private static final ThreadLocal<AndroidDriver> DRIVER_POOL = new ThreadLocal<>();

    public static AndroidDriver getWebDriver(DesiredCapabilities capabilities) throws MalformedURLException {
        if (DRIVER_POOL.get() == null) {
            synchronized (LocalDriverManager.class) {
                if (DRIVER_POOL.get() == null) {
                    log.info("Setting driver to DRIVER_POOL.");
                    DRIVER_POOL.set(createDriver(capabilities));
                }
            }
        }
        log.info("Getting AndroidDriver from DRIVER_POOL.");
        return DRIVER_POOL.get();
    }

    public static void closeDriver() {
        log.info("Try to close AndroidDriver.");
        if (DRIVER_POOL.get() != null) {
            DRIVER_POOL.get().quit();
            DRIVER_POOL.remove();
            log.info("AndroidDriver was closed.");
        }
    }
}