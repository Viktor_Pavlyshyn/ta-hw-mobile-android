package ua.com.epam.pageobject;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.MalformedURLException;

@Getter
public class GmailLoginPage extends AbstractGmailPage {
    @AndroidFindBy(id = "welcome_tour_got_it")
    private MobileElement buttonAccept;
    @AndroidFindBy(id = "setup_addresses_add_another")
    private MobileElement buttonAddMail;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout")
    private MobileElement tabLogInGmail;
    @AndroidFindBy(className = "android.widget.EditText")
    private MobileElement inputMail;
    @AndroidFindBy(xpath = "//android.view.View[4]/android.view.View/android.widget.Button")
    private MobileElement buttonMail;
    @AndroidFindBy(className = "android.widget.EditText")
    private MobileElement inputPassword;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.view.View/android.widget.Button")
    private MobileElement buttonPassword;
    @AndroidFindBy(xpath = "//android.view.View[4]/android.view.View/android.widget.Button")
    private MobileElement buttonAcceptRules;
    @AndroidFindBy(id = "owner")
    private MobileElement firstMail;
    @AndroidFindBy(id = "com.google.android.gms:id/sud_navbar_more")
    private MobileElement buttonScrollService;
    @AndroidFindBy(id = "com.google.android.gms:id/sud_navbar_next")
    private MobileElement buttonAcceptService;
    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement goToGmail;

    public GmailLoginPage() throws MalformedURLException {
        super();
    }

    public void waitInputPassAndSendKeys(String value) {
        fwait.until(ExpectedConditions.elementToBeClickable(buttonPassword));
        inputPassword.sendKeys(value);
    }

    public void scrollToAcceptRulesAndPress() {
        driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector())" +
                ".scrollIntoView(new UiSelector().text(\"" + "I agree" + "\"));"));

        fwait.until(ExpectedConditions.elementToBeClickable(buttonAcceptRules)).click();
    }

    public void waitToBeClickableButtonMailAndPress() {
        fwait.until(ExpectedConditions.elementToBeClickable(buttonMail)).click();
    }

    public void waitReadyPageAndPressGoToGmail() {
        fwait.until(ExpectedConditions.elementToBeClickable(firstMail));
        goToGmail.click();
    }
}
