package ua.com.epam.pageobject;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

import java.net.MalformedURLException;

@Getter
public class GmailComposePage extends AbstractGmailPage {
    @AndroidFindBy(id = "to")
    private MobileElement inputRecipient;
    @AndroidFindBy(id = "subject")
    private MobileElement inputTopic;
    @AndroidFindBy(id = "send")
    private MobileElement buttonSent;

    public GmailComposePage() throws MalformedURLException {
        super();
    }
}
