package ua.com.epam.pageobject;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.MalformedURLException;

@Getter
public class GmailAccountSettingPage extends AbstractGmailPage {
    @AndroidFindBy(id = "og_apd_ring_view")
    private MobileElement accountGmail;
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]")
    private MobileElement manageAccount;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.widget.RelativeLayout")
    private MobileElement accountGoogle;
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"More options\"]")
    private MobileElement settingGoogle;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[2]")
    private MobileElement deleteAccount;
    @AndroidFindBy(id = "android:id/button1")
    private MobileElement confirmDelete;

    public GmailAccountSettingPage() throws MalformedURLException {
        super();
    }

    public void waitToBeClickableSettingGoogleAndPress() {
        fwait.until(ExpectedConditions.elementToBeClickable(settingGoogle));
        settingGoogle.click();
    }
}
