package ua.com.epam.pageobject;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import ua.com.epam.capbility.CapabilitiesFactory;
import ua.com.epam.device.DevicesManager;
import ua.com.epam.driver.LocalDriverManager;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.NoSuchElementException;

public abstract class AbstractGmailPage {
    protected AndroidDriver driver;
    protected Wait<WebDriver> fwait;

    public AbstractGmailPage() throws MalformedURLException {
        this.driver = LocalDriverManager.getWebDriver(
                CapabilitiesFactory.getGmailLogin(
                        DevicesManager.createMobile()));
        this.fwait = createFluentWait();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    private Wait<WebDriver> createFluentWait() {
        return new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
    }
}
