package ua.com.epam.pageobject;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;

import java.net.MalformedURLException;

@Getter
public class GmailSentMessagePage extends AbstractGmailPage {
    @AndroidFindBy(id = "viewified_conversation_item_view")
    private MobileElement sentMessage;
    @AndroidFindBy(id = "delete")
    private MobileElement buttonDeleteMsg;

    public GmailSentMessagePage() throws MalformedURLException {
        super();
    }
}
