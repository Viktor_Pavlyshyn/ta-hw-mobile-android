package ua.com.epam.pageobject;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.MalformedURLException;

@Getter
public class GmailHomePage extends AbstractGmailPage {
    @AndroidFindBy(id = "compose_button")
    private MobileElement buttonCompose;
    @AndroidFindBy(className = "android.widget.ImageButton")
    private MobileElement tabMenu;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[7]")
    private MobileElement tadSent;
    @AndroidFindBy(id = "android:id/button1")
    private MobileElement cancelMeet;

    public GmailHomePage() throws MalformedURLException {
        super();
    }

    public void waitToBeClickableTabMenuAndPress() {
        fwait.until(ExpectedConditions.elementToBeClickable(tabMenu)).click();
    }

    public void waitCancelMeetAndSendKeys() {
        fwait.until(ExpectedConditions.elementToBeClickable(cancelMeet));
        cancelMeet.click();
    }
}
