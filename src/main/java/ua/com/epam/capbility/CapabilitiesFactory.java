package ua.com.epam.capbility;

import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import ua.com.epam.model.Mobile;

public class CapabilitiesFactory {

    public static DesiredCapabilities getGmailLogin(Mobile mobile) {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, mobile.getPlatformName());
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, mobile.getDeviseName());
        capabilities.setCapability(MobileCapabilityType.UDID, mobile.getUdid());
        capabilities.setCapability("appPackage", mobile.getAppPackage());
        capabilities.setCapability("appActivity", mobile.getAppActivity());
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, mobile.getComponentTimeOut());

        return capabilities;
    }
}
