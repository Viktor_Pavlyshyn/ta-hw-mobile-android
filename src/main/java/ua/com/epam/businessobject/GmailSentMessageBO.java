package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailSentMessagePage;

import java.net.MalformedURLException;

@Log4j2
public class GmailSentMessageBO {
    private final GmailSentMessagePage sentMessagePage;

    public GmailSentMessageBO() throws MalformedURLException {
        this.sentMessagePage = new GmailSentMessagePage();
    }

    @Step("Get all text from message.")
    public String getTextMsg() {

        log.info("Getting all text from message.");

        return sentMessagePage.getSentMessage().getText();
    }

    @Step("Delete message.")
    public GmailSentMessageBO deleteMsg() {

        log.info("Deleting message.");
        sentMessagePage.getSentMessage().click();
        sentMessagePage.getButtonDeleteMsg().click();

        return this;
    }
}
