package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailHomePage;

import java.net.MalformedURLException;

@Log4j2
public class GmailHomeBO {
    private final GmailHomePage gmailHomePage;

    public GmailHomeBO() throws MalformedURLException {
        this.gmailHomePage = new GmailHomePage();
    }

    @Step("Pass to write letter.")
    public GmailComposeBO composeMsg() throws MalformedURLException {

        log.info("Click by button 'Write'.");
        gmailHomePage.getButtonCompose().click();

        try {
            log.info("Try to cancel 'Meet'.");
            gmailHomePage.waitCancelMeetAndSendKeys();
        } catch (Exception e) {
            log.warn("Can't cancel 'Meet'.");
        }

        return new GmailComposeBO();
    }

    @Step("Pass to sent letter.")
    public GmailSentMessageBO toSentLetter() throws MalformedURLException {

        log.info("Passing to sent letter.");
        gmailHomePage.waitToBeClickableTabMenuAndPress();
        gmailHomePage.getTadSent().click();

        return new GmailSentMessageBO();
    }
}
