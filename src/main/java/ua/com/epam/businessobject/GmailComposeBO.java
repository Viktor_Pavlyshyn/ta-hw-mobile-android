package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailComposePage;

import java.net.MalformedURLException;

@Log4j2
public class GmailComposeBO {
    private final GmailComposePage gmailComposePage;

    public GmailComposeBO() throws MalformedURLException {
        this.gmailComposePage = new GmailComposePage();
    }

    @Step("Set recipient - '{0}', topic - '{1}' and send letter.")
    public GmailComposeBO writeAndSendMessage(String recipient, String topic) {

        log.info("Setting recipient - {}.", recipient);
        gmailComposePage.getInputRecipient().sendKeys(recipient);

        log.info("Setting topic - {}.", topic);
        gmailComposePage.getInputTopic().sendKeys(topic);

        log.info("Sending letter.");
        gmailComposePage.getButtonSent().click();

        return this;
    }
}
