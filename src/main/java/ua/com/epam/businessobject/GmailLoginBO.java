package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailLoginPage;

import java.net.MalformedURLException;

@Log4j2
public class GmailLoginBO {
    private final GmailLoginPage gmailLoginPage;

    public GmailLoginBO() throws MalformedURLException {
        this.gmailLoginPage = new GmailLoginPage();
    }

    @Step("Go to the login page.")
    public GmailLoginBO goToLoginGoogle() {

        log.info("Going to the login page.");
        gmailLoginPage.getButtonAccept().click();
        gmailLoginPage.getButtonAddMail().click();
        gmailLoginPage.getTabLogInGmail().click();

        return this;
    }

    @Step("Set login - '{0}' and password - '{1}'.")
    public GmailHomeBO loginToGmail(String login, String password) throws MalformedURLException {

        log.info("Setting login - {}.", login);
        gmailLoginPage.getInputMail().sendKeys(login);
        gmailLoginPage.waitToBeClickableButtonMailAndPress();

        log.info("Setting password.");
        gmailLoginPage.waitInputPassAndSendKeys(password);
        gmailLoginPage.getButtonPassword().click();

        return acceptRules();
    }

    @Step("Accept google rules.")
    public GmailHomeBO acceptRules() throws MalformedURLException {

        log.info("Accepting rules.");
        gmailLoginPage.scrollToAcceptRulesAndPress();
        gmailLoginPage.getButtonScrollService().click();
        gmailLoginPage.getButtonAcceptService().click();
        gmailLoginPage.waitReadyPageAndPressGoToGmail();

        return new GmailHomeBO();
    }
}
