package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailAccountSettingPage;

import java.net.MalformedURLException;

@Log4j2
public class GmailAccountSettingBO {
    private final GmailAccountSettingPage accountSettingPage;

    public GmailAccountSettingBO() throws MalformedURLException {
        this.accountSettingPage = new GmailAccountSettingPage();
    }

    @Step("Delete google account.")
    public void deleteAccountGmail() {

        log.info("Deleting google account.");
        accountSettingPage.getAccountGmail().click();
        accountSettingPage.getManageAccount().click();
        accountSettingPage.getAccountGoogle().click();
        accountSettingPage.waitToBeClickableSettingGoogleAndPress();
        accountSettingPage.getDeleteAccount().click();
        accountSettingPage.getConfirmDelete().click();
    }
}
