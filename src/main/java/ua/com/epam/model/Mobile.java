package ua.com.epam.model;

import lombok.Data;

@Data
public class Mobile {
    private String platformName;
    private String deviseName;
    private String udid;
    private String appPackage;
    private String appActivity;
    private String componentTimeOut;
}
